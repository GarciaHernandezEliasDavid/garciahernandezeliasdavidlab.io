var vision = (function() {
  var key_api = "AIzaSyBY0aXaxLFW44mcEPxrYJ4yqP5hXe6pF9Y";
  var URL_Vision = "https://vision.googleapis.com/v1/images:annotate?key="+key_api;
  var URL_translate = "https://translation.googleapis.com/language/translate/v2?key="+key_api;
  var URL_translateLen = "https://translation.googleapis.com/language/translate/v2/languages?key="+key_api;
  var URL_YT = "https://www.googleapis.com/youtube/v3/search?";
  var arrVideos=[]; //arr videos que coinciden con geolocalizacion
  var id_videos_aqui= [];//arreglo de arreglo para mi paginacion
  var uri_img; //url de img
  var next_token; //token de paginacion en list
  var prueba;
  var contadorPeticiones = 0; //contador de peticiones
  var map;
  var nume_pagina=0;//representa el numero de pagina (psicion en el arreglo id_videos_aqui)
  var numero_pag;//cantidad de videos solicitados usado solo cuando son mayor a 50
//------------------                      generales

  var _sendRequest = function(metodo,request_json,url,funcion){
      var xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {      
              funcion(this.responseText);     
            } else {
              //mensaje.textContent = "Error " + this.status + " " + this.statusText + " - " + this.responseURL;
              console.log(this.responseText);
              prueba =JSON.parse(this.responseText) ;
            }
        }
      };
      xhr.open(metodo, url, true);
      xhr.send(request_json);
  };

  var _createElementHTML = function(type,array_attributes,idd){
    var elemento = document.createElement(type);
    //var a = [{key:"b" , value:"b"},{key:"b2" , value:"b2"},{key:"b3" , value:"b3"}];
    if(idd !== ""){
        elemento.id = idd;
    }
    for (var i = array_attributes.length - 1; i >= 0; i--) {
      elemento.setAttribute(array_attributes[i].key,array_attributes[i].value);      
    }
      return elemento;
  };
//          ---                           API VISION
  var _encodeImageFileAsURL = function(){
      var filesSelected = document.getElementById("input").files;
      console.log(filesSelected);
      if (filesSelected.length > 0) {
          var fileToLoad = filesSelected[0];
          var fileReader = new FileReader();
          fileReader.onload = function(fileLoadedEvent) {
              var srcData = fileLoadedEvent.target.result; // <--- data: base64
             //var contenedor = document.getElementById("upload_image");
              uri_img = srcData;
              var txt2 = srcData.split(",");
              _sendRequest('POST',_createJson(txt2),URL_Vision,_APIVISION);              
          }
          fileReader.readAsDataURL(fileToLoad);
      }else{
        alert("error seleccione una imagen");
      }
  };
  var _APIVISION = function(Json_responseText){
          var archivo_descargado = document.getElementById("cont_2");

          if(archivo_descargado.children.length>1){  
             archivo_descargado.removeChild(archivo_descargado.children[1])
          }
          var dvi1 = _createElementHTML("div",[{key:"class" , value: "col s12 m6"}],"");
          var dvi2 = _createElementHTML("div",[{key:"class" , value: "card blue-grey darken-1"}],"");
          var dvi3 = _createElementHTML("div",[{key:"class" , value: "card-content white-text "}],"");
          var mensaje = _createElementHTML("p",[{key: "class",value: "card-title"}],"texto_img");
          mensaje.textContent = "";
          var imagen = _createElementHTML("img",[{key:"src" , value: uri_img},{key:"class" , value: "responsive-img pure-img"}]);
          var myObj = JSON.parse(Json_responseText);
          mensaje.textContent = myObj.responses[0].webDetection.webEntities[0].description ;          
          dvi3.appendChild(mensaje);
          dvi3.appendChild(imagen);
          dvi2.appendChild(dvi3);
          dvi1.appendChild(dvi2);
           archivo_descargado.appendChild(dvi1);
          //mandar a llamar funcion para agregar elemntos para traducir
          _sendRequest('POST',_createJsonLenguages(),URL_translateLen,_addLenguages);

  };

  var _createJson = function(arr){
      var json = '{' +
      ' "requests": [' +
      ' { ' +
      '   "image": {' +
      '     "content":"' + arr[1] + '"' +
      '   },' +
      '   "features": [' +
      '       {' +
      '         "type": "WEB_DETECTION",' +
      '       }' +
      '   ]' +
      ' }' +
      ']' +
      '}';
      return json;
  }
//                    ---                 API TRANSLATE
  var _createJsonLenguages = function(){
    var json = '{' +
    '"target": "es"' +
    '}';
    return json;
  };

  var _createJsonTranslate = function(texto,Idioma){
    var json = '{' +
    '"q":' + '"'+ texto +'",'
    +'"target": "' + Idioma +'"}';
    return json;
  };

  var _addLenguages = function(ArrJSON){// identificacion de un idioma para formar JSON    
    var selector = document.getElementById("dropdown2");
    var arreglo_lenguages = JSON.parse(ArrJSON);
    console.log("idiomasTranslate");
    for (var i = arreglo_lenguages.data.languages.length - 1; i >= 0; i--) {
      var new_li = _createElementHTML("li",[{key:"valorTrans" ,value: arreglo_lenguages.data.languages[i].language }],"");
      var new_a = _createElementHTML("a",[],"");
      new_a.textContent = arreglo_lenguages.data.languages[i].name;
      new_li.appendChild(new_a);
      selector.appendChild(new_li);
    }
  };

  var _requesttraducir = function(evento){
    var contenedor_texto = document.getElementById("texto_img");
    var texto = contenedor_texto.textContent;
    console.log(evento.originalTarget);
    texto=texto.replace("\n"," ");
    console.log(texto);
    var idioma = evento.originalTarget;
    console.log(idioma.parentNode.getAttribute("valorTrans"));
    _sendRequest('POST',_createJsonTranslate(texto,idioma.parentNode.getAttribute("valorTrans")),URL_translate,_addResponseTranlate);
  };

  var _iniciar = function(){ //funcion principal agregar listener 
    console.log("....iniciando");
    var boton_descargar = document.getElementById("dropdown2");
    boton_descargar.addEventListener('click', _requesttraducir, false); 
    var boton_2 = document.getElementById("enviar");   
    boton_2.addEventListener('click', _encodeImageFileAsURL, false); 
    var boton_yt = document.getElementById("search-button");      
    boton_yt.addEventListener('click',_Request_YT,false);
    var boton_pag = document.getElementById("next_pag");
    var contenedor = document.getElementById("cont-imgs");
    contenedor.addEventListener('click',_trow_modal,false);
    //boton_pag.addEventListener('click',function(evento){console.log(evento.originalTarget)},false);
  };
  
  var _addResponseTranlate = function(ArrJSON){
      var traduccion = JSON.parse(ArrJSON);
      console.log("recibido....");
      var texto = document.getElementById("texto_img");
      texto.textContent = "";
      texto.textContent = traduccion.data.translations[0].translatedText;
  };
// API YOUTUBE
  var _URLyt = function(text_search,next_token,max_resultados){//search
    var URI_request = URL_YT
        + "key="+key_api
        + "&part=snippet"
        + "&maxResults="+ max_resultados
        + "&order=date"
        + "&type=video"
        + "&fields=nextPageToken,prevPageToken,pageInfo,items(id,snippet(title,publishedAt,description,thumbnails/high))"
        + "&q=" + text_search;
      if(next_token){
          URI_request += "&pageToken=" + next_token;
       }        
    return URI_request;
  };
  var _URLyt_id = function(ids){//solocitud a youtube.videos con los ids de los videos devueltos por search maximo 50
    var ur_l = "https://www.googleapis.com/youtube/v3/videos?"
        + "part=snippet,recordingDetails,statistics"
        + "&id="+ids
        + "&fields=items(id,snippet(title,publishedAt,description,thumbnails/high),recordingDetails,statistics)"
        + "&key="+key_api;
    return ur_l;
  };
  //metodo es get,post,put   url llamar a alguna de las dos funciones _URLyt , _URLyt_id  funcion es la funcion que se encarga de procesar la respuesta 
    var _sendRequest2 = function(metodo,url,funcion){
      var xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {   
              funcion(this.responseText);     
            } else {
              //mensaje.textContent = "Error " + this.status + " " + this.statusText + " - " + this.responseURL;
              console.log(this.responseText);
            }
        }
      };
      xhr.open(metodo, url, true);
      xhr.send();
  };

  var _Request_YT = function(){
    arrVideos=[];
    id_videos_aqui=[];
    nume_pagina=0;
    var t_numero = document.getElementById("num_busquedas");
    var numero = Number(t_numero.value);
    if( numero !== NaN && numero > 0){
     var texto = document.getElementById("texto_img"); 
      console.log(texto);
      if(texto){
        var bt_next = document.getElementById("next_pag");
        var bt_next2 = document.getElementById("before_pag");
        bt_next.addEventListener('click',_pag_boton,false);        
        bt_next2.addEventListener('click',_before_boton,false);
        bt_next.disabled  = false;
        bt_next2.disabled  = false;
        if(numero >50){//paginacion de YT
          numero_pag= numero;
          _paginacion_masde50("");
        }else{//paginacion Mia <=50                     
          _sendRequest2('GET',_URLyt(texto.textContent,"",numero),_paginacion);//solicitud principal <= 50 videos
        }
        //llamar para visualizar los datos
      }else{
        alert("Seleccine una imagen antes de continuar");
      }
    }else{
      alert("ingrese un numero");
    }

  };
  var _pag_boton = function(){
    console.log("next bton");   
    if(nume_pagina < id_videos_aqui.length - 1){
      nume_pagina++;
      _addItems(id_videos_aqui[nume_pagina]);

    }
  }
  var _before_boton = function(){
    console.log("before bton");  
    if(nume_pagina > 0 ){
      nume_pagina--;
      _addItems(id_videos_aqui[nume_pagina]);
    }
  }
  //RECURSIVIDAD ALWAYS --- mantiene las solicitudes a search de 50 en 50 
  //entre _paginacion_masde50 _paginacin_50mas
  var _paginacion_masde50 = function(token_pag){//numero mayor a 50 ---- token =  token de paginacion YT
    if(numero_pag > 0){//variable global para manejo de la cantidad de videos
        numero_pag-=50;
        var texto = document.getElementById("texto_img");
        _sendRequest2("GET",_URLyt(texto.textContent,token_pag,50),_paginacin_50mas);
    }else{//si termino de buscar sale para visualizar los videos 
      _addItems(id_videos_aqui[nume_pagina]);
    }

  }
  //funcion para manejar los datos de respuesta, todo los agrega al arreglo de arreglos id_videos_aqui
  var _paginacin_50mas = function(datos){
    var arrVideos2 =JSON.parse(datos);
    console.log(arrVideos2);
    var total = arrVideos2.items.length;
    input_maximo_sig=Math.ceil(total/10);   
    //dividir la cantidad /10 y redondear para crear un arreglo(a) de arreglos(b)   
    //b este arreglo contiene maximo 10 videos 
    //a contiene n arreglos b
    var n = 0;
    for (var i = 0; i < input_maximo_sig ; i++) {//la cantidad de arreglos(b) dentro de (a)
      var newx =  [];
      for (var ix = 0; ix <10; ix++) {  //agrega a newx (b) la informacion de maximo 10 videos
        if (typeof arrVideos2.items[n] === 'undefined') { //evitar undefined en el arreglo (b) en caso que pida 15,25,35 etc etc
        }else{      
          newx.push(arrVideos2.items[n]);
        }
        n++;
      }

      id_videos_aqui.push(newx);//id_videos_aqui el arreglo de arreglos cada uno de lso arreglos dentro es una pagina 
    }
    if(arrVideos2.nextPageToken){
      console.log("pag mas de 50 ");
      console.log(arrVideos2.nextPageToken);
      _paginacion_masde50(arrVideos2.nextPageToken);
    }else{
      _addItems(id_videos_aqui[nume_pagina]);
    }
    
  }

  //paginacion menos de 50 elemntos
  var _paginacion = function(datos){//recibe el JSON de videos.search   
    console.log("mi paginacion");    
    var arrVideos2 =JSON.parse(datos);
    console.log(arrVideos2);
    var total = arrVideos2.items.length;
    input_maximo_sig=Math.ceil(total/10);   
    //dividir la cantidad /10 y redondear para crear un arreglo(a) de arreglos(b)   
    //b este arreglo contiene maximo 10 videos 
    //a contiene n arreglos b
    var n = 0;
    for (var i = 0; i < input_maximo_sig ; i++) {//la cantidad de arreglos(b) dentro de (a)
      var newx =  [];
      for (var ix = 0; ix <10; ix++) {  //agrega a newx (b) la informacion de maximo 10 videos
        if (typeof arrVideos2.items[n] === 'undefined') { //evitar undefined en el arreglo (b) en caso que pida 15,25,35 etc etc
        }else{      
          newx.push(arrVideos2.items[n]);
        }
        n++;
      }

      id_videos_aqui.push(newx);//id_videos_aqui el arreglo de arreglos cada uno de lso arreglos dentro es una pagina 
    }
    _addItems(id_videos_aqui[nume_pagina]);
    //mandar a llamar a la funcion que los agrega en la tabla num_pagina es usado 
    //para la paginacion cada elemento dentro de id_videos_aqui representa una "pagina"
  }
  //agregar videos 
  var _addItems = function(datos){//agregar variable arreglo de datos(respuesta YT), num define los videos que se cargaran del arreglo no los videos solicitados a yt
    console.log(datos);
    var contenedor = document.getElementById("cont-imgs");
    var clase_img;
    if(contenedor.children.length > 0){
        contenedor.removeChild(contenedor.children[1]);
        contenedor.removeChild(contenedor.children[0]);
    }
      if(datos.length <= 3){
        clase_img = "col s4 center-align"
      }else{
        clase_img = "col s2 center-align"
      }
      var div_row =_createElementHTML("div",[{key:"class" ,value: "row center-align"}],"");
      var div_row2 =_createElementHTML("div",[{key:"class" ,value: "row center-align"}],"");
      for (var i = 0 ; i <= datos.length - 1; i++) {     
        var div_html = _createElementHTML("div",[{key:"class" ,value: clase_img}],"");
        var img_html = _createElementHTML("img",[{key:"src",value: datos[i].snippet.thumbnails.high.url},{key:"class" ,value: "imgIcon responsive-img modal-trigger center-align"},{key:"id_video" ,value: datos[i].id.videoId},{key:"href" ,value: "#modal1"}],"");
        div_html.appendChild(img_html);
        if( i < 5 ){              
           div_row.appendChild(div_html); 
        }else{
          div_row2.appendChild(div_html);
        }                 
      }

      contenedor.appendChild(div_row); 
      contenedor.appendChild(div_row2);
      console.log(_ids_yt_video(datos));
      var cadena_ids = _ids_yt_video(datos);
      //hacer la solicitud a yotube.videos para comprobar coordenadas y poner marcadores en el mapa
      _sendRequest2("GET",_URLyt_id(cadena_ids),_validarGeo); //_validarGeo funcion que recibe los datos verificar si tiene coordenadas y poner marcadores
  }

  var _ids_yt_video = function(datos){//retorna la cadena de id´s para hacer la peticion en youtube.videos para checar si tienen coordenadas 
    var ids_yt="";
    for (var i = 0 ; i <= datos.length - 1; i++) {       
       if(i < datos.length -1 ){          
          ids_yt += datos[i].id.videoId+",";
       }else{
          ids_yt += datos[i].id.videoId;
       }
     }
     return ids_yt;
  }
  //funcion que comprueba la geolocaliza
  var _validarGeo = function(r_datos){
    var datos = JSON.parse(r_datos);
    console.log("coordenadas");
 
    for (var i = 0 ; i < datos.items.length; i++) {
      if(datos.items[i].recordingDetails){
        if(datos.items[i].recordingDetails.location){
          if(datos.items[i].recordingDetails.location.latitude && datos.items[i].recordingDetails.location.longitude){
            
            var marker = new google.maps.Marker({
              position: { lat: datos.items[i].recordingDetails.location.latitude ,
                          lng: datos.items[i].recordingDetails.location.longitude },
              map: map,
              title: datos.items[i].id
            });
            marker.addListener('click', toggleBounce);
          }        
        }        
      }
    }
  }
//cargar mapa
  var _mostarMapa = function(){
    map = new google.maps.Map(document.getElementById('map'), {
          zoom: 2,
          center: {lat: 30.2513675, lng: -33.7050631}
        });
  }
    //evento en marker 
  var toggleBounce =  function(evento){
    console.log("evento marcador");
    console.log(evento.Fa.originalTarget);
    var marker = evento.Fa.originalTarget;
    var video_url =  "//www.youtube.com/embed/" + evento.Fa.originalTarget.title;
    var video = document.getElementById("resp_video");
    video.setAttribute("src",video_url);
    var marcador = evento.Fa.originalTarget;
    marcador.setAttribute("class","modal-trigger");
    marcador.setAttribute("href","#modal1");
  }

//modal para visualizar el video con toda su informacion
  var _trow_modal = function(evento){    
    console.log("trow modal");
    console.log(evento.originalTarget);// cambiar para clic en una imagen comifique url
    var id_v = evento.originalTarget.getAttribute("id_video");
    var video_url =  "//www.youtube.com/embed/" + id_v;
    var video = document.getElementById("resp_video");
    video.setAttribute("src",video_url);
  };
//iniciar
    return {
        "iniciar": _iniciar,
        "mostrarMapa": _mostarMapa
    };

} )();